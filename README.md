# Remote Port Documentation:

This document attempts to describe the work that our team has done to work on developing an external remote port protocal implementation. While the Remote-Port protocal is open-source, it is only in source and there is little to no documentation for it. As such, we have described our outcomes and attempted to create our own documentation here.

## Demo
Our demo attempts to take a system composed of 2 major parts. We have implemented a dummy device in SystemC that is attached to a QEMU instance running a version of Buildroot linux. This is novel in that all examples produced by Xilinx are done using Petalinux. We choose Buildroot due to its simplicity and ease of use over Petalinux. We then also have an application `rp-demo` running on our host that can be controlled by the user. Both the QEMU instance and the Host application create Unix sockets backed by the Remote-Port protocal. Our SystemC design then waits for connection on both of those sockets prior to continuing execution. This in theory allows us to send and recieve data packets from our host application, and read the data in QEMU. This allows for the use case of continually pipping in data to our simulation without the need to reset or fully implement the SystemC implementation. This is important in the Co-Simulation methodology, as you may have mocked hardware or software implementations providing realistic interactions to one another.

### Compiling and Running The Demo
To start, you must first clone and seperately compile `SystemC 2.3.2` for your system. This will then be used to link aginst when compiling our demo application. Then, edit the `Makefile` variable such that `SYS_C = <path/to/your/systemc/dir>`. This will ensure that the paths are correct when compling and linking the demo. The following makefile commands are then available:
- `build`:
    Build the entire project including the included Xilinx Co-simulation demos.
- `rebuild`:
    Clean and build the entire project (See `build`).
- `build_zynq_demo`:
    Build only our demo based on the Zynq SoC. This is the most slim build profile.
- `clean`: 
    Clean all generated binaries and intermediate files created during the build process.
- `run`:
    Build the entire project and print out the commands needed to run the demo in order. This will generate the specific full-length paths needed for your system and is the recomended way to build and run the demo. Ensure that you run the commands in the order printed or SystemC will hang waiting to connect to the prorvided sockets.

## Protocal
The Remote-Port protocal is a protocal developed by Xilinx to communicate using Transactional Level Modeling (TLM) between two hardware devices for simulation and emulaiton. As such, it takes into account syncing of simulaiton steps, the capabilites of the other device, and allows for any number or hosts and devices to be attached togeather.

The data itself can be transmitted over Unix sockets, TCP, or TCPD socket protocals, but for our purposes we use strictly unix sockets when communicating with our SystemC implementation.

### __Packets__
#### __Common Structures__:
The remote-port packet sturcture contains some common structures listed below:

- __Header Struct__ [`rp_pkt_hdr`]:

    This is a generic packet header that is included and used in every every packet. It contains basic info about the packet being sent (used for decoding), the length of the entire packet, the packet ID, and thr originating device.
    
    ```c
        // Header of all packets sent over the remote-port
    struct rp_pkt_hdr {
        uint32_t cmd;   // Command/type of packet being sent
        uint32_t len;   // Total length of the packet including this head
        uint32_t id;    // The packet id, I belive unique but just used for book keeping, not functionality
        uint32_t flags; // ???
        uint32_t dev;   // The originating device
    } PACKED;
    ```

- __Remote-Port Packet__ [`rp_pkt`]:

    This generic packet struct union is used to handle packets prior to decoding the header. It is used as a generic wrapper for packet handling.

    ```c
    // Generic Remote-Port Packet
    struct rp_pkt {
        union {
            struct rp_pkt_hdr hdr;              // Not sure about use cases for only sending a header, possibly NOP packets
            struct rp_pkt_hello hello;          // Hello Packet
            struct rp_pkt_busaccess busaccess;  // Read + Write Packets
            struct rp_pkt_busaccess_ext_base busaccess_ext_base;                // Extended Read + Write Packets
            struct rp_pkt_interrupt interrupt;  //Interrupt Packet
            struct rp_pkt_sync sync;            // Sync Packet
        };
    };
    ```

- __Peer Structure__ [`rp_peer_state`]:

    The Peer State structure is intended to be used by a device to track its peer on the other side of the remote port. This is normally populated follwing the exchange of hello packets and contains basic information about the peer, required when sending and encoding packets to be sent. While not extensive, it allows for basic bookkeeping and tracking of the peer deivce.

    ```c
    struct rp_peer_state {
        void *opaque;

        struct rp_pkt pkt;              // Previous packet recieved
        bool hdr_used;                  // If a header was present

        struct rp_version version;      // Remote port version struct, used to ensure that
                                        // both are not out of version sync, haulting communication

        // Capabilites of the peer
        // Used when encoding packets to decide if legacy structures need to be supported
        struct {
            bool busaccess_ext_base;
            bool busaccess_ext_byte_en;
            bool wire_posted_updates;
            bool ats;
        } caps;

        /* Used to normalize our clk.  */
        int64_t clk_base;

        // Configuration packets were not explored in our project and were
        // left unimplemented in handling functions of the libremoteport library.
        struct rp_cfg_state local_cfg;
        struct rp_cfg_state peer_cfg;
    };
    ```

#### __Data Packets__
The Remote-Port protocal implements the following packets:

- __Hello__ [`RP_CMD_hello`]:

    This packet is intended an an ititial hello sent between devices. Its implementation can be seen below as specified in the `remote-port-proto.h` header. It is sent as the initial packet over the remote port to provide peer data about the device on the other side of the interface. This can include capabilites, identifying ids, and other information. This is also used to popluate the _Peer_ structure provided above. It is used to keep track of the other devices on the port and addressing and encoding data for a wide range of devices with posisbly different capabilites.
    
    __Definition__
    ```c
    // Version struct to ensure that both devices are using the same version of remote-port
    struct rp_version {
        uint16_t major; // Major remote-port version of originating device (RP_VERSION_MAJOR)
        uint16_t minor; // Minor remote-port version of originating device (RP_VERSION_MINOR)
    } PACKED;

    // The supported additional capabilites such as extenxed bus access packet support
    struct rp_capabilities {
        /* Offset from start of packet.  */
        uint32_t offset;    // Capabilites are offset/sent immediately after the packet data, this is the offset of the packet data to their start
        uint16_t len;       // Length of the capabilites to be included
        uint16_t reserved0; // Reseverd & unused value
    } PACKED;

    // Hello packet
    struct rp_pkt_hello {
        struct rp_pkt_hdr hdr;          // Packet header
        struct rp_version version;      // Remote-Port Version Definition
        struct rp_capabilities caps;    // Device capabilities meta-data
    } PACKED;
    ```

- __Sync__ [`RP_CMD_sync`]:

    Sync packets are used to sync the SystemC simulation environment with the application at the other end of the remote-port. This allows for ensuring that actions are taken in sync with each other. Within SystemC, you can define these devices as `remoteport_tlm_sync_untimed` or `remoteport_tlm_sync_loosely_timed`. 
    
    Untimed devices are an extension of the loosely timed definition, as techically a device cannot be untimed. The device is assumed to have no sense of time and thus is not attempted to be synced with and will not stall the simulation environment. There is a sense of the drift in simulation time between the environments refered to as _quantum_. This refers to the amount of time that can pass without a syncing of the devices. Normally, this would stall the simulation while one device waits for the other to catch up and update it on its current simulation status. With an untimed device, this does not occur.

    Losely timed devices implement this _quantum_ checkin mechanism in order to remain loosely in sync with one another. This allows for both devices to simulate freely, knowing that they are never outside of a specified distance from one another. This seems to be the defacto implementation, as in a realistic simulation, ensuring that your multi-device landscape is on the same page is a primary concern in ensuring the validity of your simulations.

    There is also a notion of strictly timed deives, although these were not explored. It is assumed that these ensure constant distances in simulation or even in sequence simulation to a specified timestep. While some applciations may require this stringent management, most general simuations can achieve a similar effect by reducing the _quantum_ value prior to simulation.

    __Definition__
    ```c
    struct rp_pkt_sync {
        struct rp_pkt_hdr hdr;  // Stanard header
        uint64_t timestamp;     // Timestamp of simulated environemtn of the header
    } PACKED;
    ```

- __Bus Access__ (Read & Write) [`RP_CMD_read`, `RP_CMD_write`]:

    Buss Access packets are the base of reading and writing data to the theoretical signal bus remote port is implementing. These packets are used as a sort of header to the data which is written directly following this packet in the case of writes. Each is then suppleid a response packet from the peer, either certifying the reception of the packet or returning the requested data in the case of a read. The reponse packets vary slightly and can be found in the `remote-port-proto.h` file in the `libremoteport` directory.

    The `rp_encode_busaccess_in` struct is used as a configuation for populating the `rp_pkt_busaccess` packets. It just serves as a flat interface for value input to the encoding function.

    __Definition__
    ```c
    struct rp_encode_busaccess_in {
        uint32_t cmd;               // Packet command (RP_CMD_read or RP_CMD_write)
        uint32_t id;                // Packet ID
        uint32_t flags;
        uint32_t dev;               // Device ID/number this packet is intended for
        int64_t clk;                // Clock value at time of encoding
        uint64_t master_id;         // ID of sender (master on the bus interface)
        uint64_t addr;              // Address to be accessed for the write or read
        uint64_t attr;
        uint32_t size;              // size of data following packet
        uint32_t width;             // bus width
        uint32_t stream_width;
        uint32_t byte_enable_len;
    };

    struct rp_pkt_busaccess {
        struct rp_pkt_hdr hdr;      // Standard packet header
        uint64_t timestamp;         // Encode timestamp
        uint64_t attributes;        
        uint64_t addr;              // Operation address (read or write location)

        /* Length in bytes.  */
        uint32_t len;               // length in bytes of all data (packet + data)

        /* Width of each beat in bytes. Set to zero for unknown (let the remote
        side choose).  */
        uint32_t width;             // Value hard coded to 0 in our uses

        /* Width of streaming, must be a multiple of width.
        addr should repeat itself around this width. Set to same as len
        for incremental (normal) accesses.  In bytes.  */
        uint32_t stream_width;

        /* Implementation specific source or master-id.  */
        uint16_t master_id;         // ID of sending device (master on the interface)
    } PACKED;
    ```

    __Extended Bus Access__ [`rp_pkt_busaccess_ext_base`]:

    The `rp_pkt_busaccess_ext_base` packet contains all the capabilites of the prvious `rp_pkt_busaccess` packet and is meant to be intercompatible for devices that do not support it. IT primarly allows for additional information about the location of data in the packet and allows for future extension if needed.

    __Definition__
    ```c
    /* This is the new extended busaccess packet layout.  */
    struct rp_pkt_busaccess_ext_base {
        struct rp_pkt_hdr hdr;      // Stanard header
        uint64_t timestamp;         // Encoding timestamp
        uint64_t attributes;
        uint64_t addr;              // Address of operation (read or write)

        /* Length in bytes.  */
        uint32_t len;               // length of entire message (packet + data)

        /* Width of each beat in bytes. Set to zero for unknown (let the remote
        side choose).  */
        uint32_t width;             // Hard coded to 0 in our uses

        /* Width of streaming, must be a multiple of width.
        addr should repeat itself around this width. Set to same as len
        for incremental (normal) accesses.  In bytes.  */
        uint32_t stream_width;

        /* Implementation specific source or master-id.  */
        uint16_t master_id;         // ID of the sending device (master on the bus interface)
        /* ---- End of 4.0 base busaccess. ---- */

        // Extended Master ID support
        uint16_t master_id_31_16;   /* MasterID bits [31:16].  */
        uint32_t master_id_63_32;   /* MasterID bits [63:32].  */
        /* ---------------------------------------------------
        * Since hdr is 5 x 32bit, we are now 64bit aligned.  */

        uint32_t data_offset;       /* Offset to data from start of pkt.  */
        uint32_t next_offset;       /* Offset to next extension. 0 if none.  */

        uint32_t byte_enable_offset;
        uint32_t byte_enable_len;

        /* ---- End of CAP_BUSACCESS_EXT_BASE. ---- */

        /* If new features are needed that may always occupy space
        * in the header, then add a new capability and extend the
        * this area with new fields.
        * Will help receivers find data_offset and next offset,
        * even those that don't know about extended fields.
        */
    } PACKED;
    ```

- __Interrupt__ [`RP_CMD_interrupt`]:
    We did not explore this packet type in-depth. It is intended for use to interrupt the the receiving device and handle some additional data to come. This could be used with either physical interrupt signals in SystemC or software interrupts that a processor might handle.

    __Definition__
    ```c
    struct rp_pkt_interrupt {
        struct rp_pkt_hdr hdr;
        uint64_t timestamp;
        uint64_t vector;
        uint32_t line;
        uint8_t val;
    } PACKED;
    ```

- __Configure__ [`RP_CMD_cfg`]:
    We did not explore this packet type in-depth. It appeares to allow the configuration and setting of options for conninucation or other components on the remote device. The handling of recieved configuration packets were not implemented at the time of development, thus they were left unexplored. The `Peer` struct does contain values for storing configurations, but they do not have a clear use case.

    __Definition__
    ```c
    // Configuration Packet
    struct rp_pkt_cfg {
        struct rp_pkt_hdr hdr;  // Packet Header
        uint32_t opt;           // Option to configure (not sure what options are allowed)
        uint8_t set;            // Value to set specified option to
    } PACKED;
    ```

- __No Operation__ [`RP_CMD_nop`]:
    This packet was sometimes seen sent from SystemC as a sort of heartbeat. Its intended purpose or other use cases were not explored/known to our team. In the backend library, this packet is often not implemented and even configured to throw errors in most cases.

.PHONY: clean build
.SUFFIXES: .o
.SECONDARY:

# Recursive Wildcard (https://stackoverflow.com/questions/2483182/recursive-wildcards-in-gnu-make/18258352#18258352)
rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

BDIR = build
BIN = bin

VOBJ_DIR = objs
VERILATED_O=$(VOBJ_DIR)/verilated.o
VFILES = apb_timer.v

SYS_C = /home/dwyer/cosim/systemc-2.3.2
LIB_SYS_C_TLM_SOC = systemctlm-cosim-demo/libsystemctlm-soc
RP_DIR = $(LIB_SYS_C_TLM_SOC)/libremote-port
DEMO_DIR = demos

FLAGS = -I $(SYS_C)/include/ -I ./$(LIB_SYS_C_TLM_SOC)/ -I ./$(DEMO_DIR)/ -L $(SYS_C)/lib-linux64/ -lsystemc -pthread -lexplain
CC_FLAGS = $(FLAGS) -g -Wall -I . -std=c++11 # -lstdc++
C_FLAGS = $(FLAGS) -g # -lstdc

EXES = $(BIN)/rp-demo $(BIN)/build-demo # $(BIN)/zynq-demo-mod

RP_SRC_C = $(call rwildcard, $(RP_DIR), *.c)
_RP_OBJS_C = $(patsubst %.c, %.o, $(RP_SRC_C))
RP_OBJS_C = $(patsubst $(RP_DIR)/%, $(BDIR)/%, $(_RP_OBJS_C))

RP_SRC_CC =  $(call rwildcard, $(RP_DIR), *.cc)
_RP_OBJS_CC = $(patsubst %.cc, %.o, $(RP_SRC_CC))
RP_OBJS_CC = $(patsubst $(RP_DIR)/%, $(BDIR)/%, $(_RP_OBJS_CC))

DEMO_SRC_CC =  $(call rwildcard, $(DEMO_DIR), *.cc)
_DEMO_OBJS_CC = $(patsubst %.cc, %.o, $(DEMO_SRC_CC))
DEMO_OBJS_CC = $(patsubst $(DEMO_DIR)/%, $(BDIR)/%, $(_DEMO_OBJS_CC))

# $(warning DEMO_SRC_CC: $(DEMO_SRC_CC))
# $(warning DEMO_OBJS_CC: $(DEMO_OBJS_CC))
# $(warning RP_SRC_CC: $(RP_SRC_CC))
# $(warning RP_OBJS_CC: $(RP_OBJS_CC))
# $(warning DEMO_SRC_CC: $(DEMO_SRC_CC))
# $(warning RP_OBJS_C: $(RP_OBJS_C))
# $(warning EXES: $(EXES))

build: build_zynq_demo $(EXES)

rebuild: clean build

#$(BIN)/rp-demo: $(DEMO_OBJS_CC) $(RP_OBJS_CC) $(RP_OBJS_C)
$(BIN)/%: $(DEMO_OBJS_CC) $(RP_OBJS_CC) $(RP_OBJS_C)
	@mkdir -p $(dir $@)
#	g++ $(CC_FLAGS) $(RP_OBJS_CC) $(RP_OBJS_C) $(BDIR)/$(notdir $@).o -o $@
	g++ -Iinc $(CC_FLAGS) $(RP_OBJS_CC) $(RP_OBJS_C) $(BDIR)/$(notdir $@).o -o $@

$(BDIR)/%.o: $(RP_DIR)/%.c
	mkdir -p $(dir $@)
	gcc -c -Iinc -o $@ $< $(C_FLAGS)

$(BDIR)/%.o: $(DEMO_DIR)/%.cc # $(DEMO_SRC_CC) $(RP_SRC_CC) $(DEMO_SRC_CC)
	mkdir -p $(dir $@)
	g++ -c -Iinc -o $@ $< $(CC_FLAGS)

$(BDIR)/%.o: $(RP_DIR)/%.cc # $(DEMO_SRC_CC) $(RP_SRC_CC) $(DEMO_SRC_CC)
	mkdir -p $(dir $@)
	g++ -c -Iinc -o $@ $< $(CC_FLAGS)

build_zynq_demo:
	make -C systemctlm-cosim-demo zynq_demo_rphost

run: build
	@echo "\n---------- QEMU ----------"
	@echo ${HOME}/cosim/qemu/aarch64-softmmu/qemu-system-aarch64 -M arm-generic-fdt-7series -m 1G -kernel ${HOME}/cosim/buildroot/output/images/uImage -dtb ${HOME}/cosim/buildroot/output/images/zynq-zc702.dtb --initrd ${HOME}/cosim/buildroot/output/images/rootfs.cpio.gz -serial /dev/null -serial mon:stdio -display none -net nic -net nic -net user -machine-path ${HOME}/cosim/buildroot/handles -icount 0,sleep=off -rtc clock=vm -sync-quantum 1000000
	@echo "\n---------- Host RP ----------"
	@echo ${PWD}/bin/rp-demo ${PWD}/sockets/rp
	@echo "\n---------- System-C ----------"
	@echo LD_LIBRARY_PATH=${HOME}/cosim/systemc-2.3.2/lib-linux64/  ${PWD}/systemctlm-cosim-demo/zynq_demo_rphost  unix:${HOME}/cosim/buildroot/handles/qemu-rport-_cosim@0  unix:${PWD}/sockets/rp 1000000

clean:
	@rm -rf $(BDIR)/*
	@make -C systemctlm-cosim-demo clean

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <string.h>

// #include <libexplain/bind.h>

#include "systemc.h"

#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/tlm_quantumkeeper.h"

#include "libremote-port/remote-port-tlm.h"

extern "C" {
    #include "libremote-port/remote-port-proto.h"
    #include "libremote-port/remote-port-sk.h"
    #include "libremote-port/safeio.h"
}

constexpr int DEBUG = 1; // Debug toggle
constexpr int MEMORY_DEVICE_ID = 2;
constexpr int OUR_MASTER_ID = 1;

char *socket_path = (char*) "./sockets/rp";

struct sockaddr_un addr;
int gen_socket, socket_descriptor, bytes_read;

struct rp_peer_state peer = {0};
bool peer_updated = false;
int pkt_id = 0;
int device = 0;
int flags = 0;
int status;

// Accept connection data
int address_len;
struct sockaddr clientaddress;

void handle_pkt(remoteport_packet &pkt) {
    // Populate peer
    if ((rp_cmd) pkt.pkt->hello.hdr.cmd == RP_CMD_hello) {
        peer_updated = true;
        peer.version = pkt.pkt->hello.version;
        pkt_id = pkt.pkt->hello.hdr.id;
        device = pkt.pkt->hello.hdr.dev;
        flags = pkt.pkt->hello.hdr.flags;
        //rp_process_caps(&peer, (void*) pkt.pkt + pkt.pkt->hello.caps.offset, pkt.pkt->hello.caps.len);
    }

    // Packet Data, print type of packet for debugging
    if (DEBUG) std::cerr << rp_cmd_to_string((rp_cmd) pkt.pkt->hello.hdr.cmd) << " packet recieved with id: " << pkt.pkt->hello.hdr.id << std::endl;

    // Set base ID if not 0
    if (pkt_id < pkt.pkt->hello.hdr.id) pkt_id = pkt.pkt->hello.hdr.id + 1;
}


// Initialize the socket for SystemC to connect
int init_socket() {
    // Check if there is an error creating the socket
    if ((gen_socket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        std::cerr << "Socket error" << std::endl;
        return -1;
    }

    // Clear address and set it to UNIX
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    if (*socket_path == '\0') {
        *addr.sun_path = '\0';
        // Set the local socket path
        strncpy(addr.sun_path+1, socket_path+1, sizeof(addr.sun_path)-2);
    } else {
        strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);
        unlink(socket_path);
    }

    // Bind the socket
    status = bind(gen_socket, (struct sockaddr*)&addr, sizeof(addr));
    if (status == -1) {
        std::cerr << "Bind error (" << status << ")" << std::endl;
        return -1;
    } else {
        std::cerr << "Bound" << std::endl;
    }

    // Listen on the socket
    status = listen(gen_socket, 5);
    if (status == -1) {
        std::cerr << "Listen error " << status << std::endl;
        exit(-1);
    } else {
        std::cerr << "Listening..." << std::endl;
    }
}


// Read data from the socket
ssize_t read_from_socket(void *buf, size_t len) {
    if (DEBUG) std::cerr << "Attempting to read " << len << " bytes" << std::endl;
    bytes_read = rp_safe_read(socket_descriptor, buf, len);
    
    // Check for reading errors
    if (bytes_read < (ssize_t) len) {
        std::cerr << "read error " << bytes_read << std::endl;
        std::cerr << strerror(errno) << std::endl;
        exit(-1);
    } else if (DEBUG) {
        std::cerr << "Data read (" << (int) len << "): ";
        for (int i = 0; i < len; i++) std:cerr << ((unsigned char*) buf)[i] << " ";
        std::cerr << std::endl;
    }

    return bytes_read;
}

// Read a packet from the socket
ssize_t read_pkt_from_socket() {
    remoteport_packet pkt;
    ssize_t bytes_read, data_len;
    unsigned char* data;

    // Read the header
    bytes_read = read_from_socket(&pkt.pkt->hdr, sizeof(pkt.pkt->hdr));
    if (bytes_read == -1) {
        std::cerr << "header read error" << std::endl;
        return -1;
    } else if (DEBUG) {
        std::cerr << "header read" << std::endl;
    }

    // Decode the header
    rp_decode_hdr(pkt.pkt);
    if (DEBUG) std::cerr << rp_cmd_to_string((rp_cmd) ((struct rp_pkt*)pkt.pkt)->hello.hdr.cmd) << " packet read with id: " << ((struct rp_pkt*)pkt.pkt)->hello.hdr.id << std::endl;
    pkt.alloc(sizeof(pkt.pkt->hdr) + pkt.pkt->hdr.len);

    // Read the body
	bytes_read = read_from_socket(&pkt.pkt->hdr + 1, pkt.pkt->hdr.len);
    if (bytes_read == -1) {
        std::cerr << "body read error" << std::endl;
        return -1;
    } else if (DEBUG) {
        std::cerr << "body read" << std::endl;
    }

    // Decode the body
    bytes_read = rp_decode_payload(pkt.pkt);
    data = pkt.u8 + sizeof(pkt.pkt->hdr) + bytes_read;
    data_len = pkt.pkt->hdr.len - bytes_read;

    // Print out data recieved
    // if (DEBUG){
    //     for (int i = 0; i < pkt.pkt->hdr.len; i ++) std::cerr << std::hex << (char*) pkt.pkt[i] << " ";
    //     std::cerr << std::endl;
    // }

    // Handle the recieved packet
    handle_pkt(pkt);

    return 0;
}

// Write data to the created socket
ssize_t write_to_socket(const void* data, size_t len) {
    // Write the packet data
    status = rp_safe_write(socket_descriptor, data, len);

    // Check for an error during writing
    if (status < (ssize_t) len) {
        std::cerr << "Write error " << status << std::endl;
        std::cerr << strerror(errno) << std::endl;
        exit(-1);
    } else if (DEBUG) {
        std::cerr << "Data written (" << len << ")" << std::endl;
    }

    return status;
}

// Send a hello packet to the other SystemC device
void send_hello() {
    // Construct and write initial required hello packet
    struct rp_pkt_hello hello_pkt = {0};
    uint32_t capabilities[] = { CAP_BUSACCESS_EXT_BASE };
    size_t cap_length = sizeof(capabilities) / sizeof(capabilities[0]);

    // Build & encode the packet
    size_t hello_size = rp_encode_hello_caps(pkt_id++, device, &hello_pkt, RP_VERSION_MAJOR, RP_VERSION_MINOR, capabilities, capabilities, cap_length);

    // Write the packet and list of capabiltes to the socket
    write_to_socket(&hello_pkt, hello_size);
    write_to_socket(capabilities, sizeof(capabilities));
    if (DEBUG) std::cerr << "Hello Sent" << std::endl;
}

// Send a sync packet to the SystemC device
// UNTESTED and UNIMPLEMENTED
void send_sync(int64_t clk) {
    struct rp_pkt_sync pkt;
    size_t len = rp_encode_sync(pkt_id++, device, &pkt, clk);
    write_to_socket(&pkt, len);
}

// Wait for a connection from SystemC after the socket has been created
int wait_for_connection() {
    // Accept and read a connection
    socket_descriptor = accept(gen_socket, (struct sockaddr *) 0, (socklen_t *) 0);
    if (socket_descriptor == -1) {
        std::cerr << "socket accept error" << std::endl;
    }

    // Say hello to the SystemC device
    send_hello();
    
    // Recieve a hello message
    status = read_pkt_from_socket();
    if (DEBUG) std::cerr << "Initial packet recieved" << std::endl;
    return status;
}


// Read a value from memory of the SystemC device
// Craft and send the Bus Access packet
// RESPONSE UNIMPLEMENTED, WILL ONLY SEND PACKET
void read_mem_value(unsigned long long addr, unsigned char* values, unsigned char width) {
    std::cerr << "Reading " << width << " bytes at 0x" << addr << std::endl;
    struct rp_pkt rp_pkt = {0};
    struct rp_encode_busaccess_in in = {0};
    remoteport_packet pkt;
    struct rp_pkt response = {0};
    size_t value_size = sizeof(unsigned char) * width;
    int resp = 1; //RP_RESP_OK;

    rp_pkt.busaccess.len = value_size;

    // Set the data used to encode the Bus Access Packet
    in.cmd = RP_CMD_read;
    in.id = pkt_id++;
    in.flags = 0;
    in.dev = MEMORY_DEVICE_ID;
    in.clk = 0;             // in.clk = sync_clk;
    in.master_id = OUR_MASTER_ID;
    in.addr = addr;
    in.attr = 0;            // |= RP_BUS_ATTR_EXT_BASE; // Not needed?
    in.size = rp_pkt.busaccess.len; // Size of entire packet + data
    in.width = 0;           // 0 means SystemC will figure it out
    in.stream_width = 0;    // Not sure
    in.byte_enable_len = 0; // Not sure

    // Allocate space for the packet
	pkt.alloc(sizeof(rp_pkt.busaccess_ext_base) + rp_pkt.busaccess.len);

    // Encode the packet with info above and our knowledge of our peer
    size_t packet_length = rp_encode_busaccess(&peer, &pkt.pkt->busaccess_ext_base, &in);

    if (DEBUG) std::cerr << "Pkt Size: " << (int) sizeof(packet_length) << std::endl;
                        //  << "Data Size: " <<  (int) value_size << std::endl
                        //  << "Total Size: " << (int) data_size << std::endl;

    // Write the packet and data to the socket
    write_to_socket(pkt.pkt, packet_length);

    // TODO: Handle responce with data values
    // Store recieved values at "*values"
}


// Write a value to the shared memory in SystemC
// Craft and encode the packet to send to the SystemC device
// VALUE DOES NOT PROPERLY PROPIGAE TO SYSTEMC
void write_mem_value(unsigned long long addr, unsigned char* values, int num_values) {
    if (DEBUG) std::cerr << "Writing " << num_values << " bytes at 0x" << addr << std::endl;
    
    // Initalize the packets
    struct rp_pkt rp_pkt = {0};
    struct rp_encode_busaccess_in in = {0};
    remoteport_packet pkt;
    struct rp_pkt response = {0};
    size_t value_size = sizeof(unsigned char) * num_values;
    int resp = 1; //RP_RESP_OK;

    rp_pkt.busaccess.len = value_size;

    // Configure packet data
    in.cmd = RP_CMD_write;
    in.id = pkt_id++;
    in.flags = 0;
    in.dev = MEMORY_DEVICE_ID;
    in.clk = 0;                     // in.clk = sync_clk;
    in.master_id = OUR_MASTER_ID;
    in.addr = addr;
    in.attr = 0;                    // |= RP_BUS_ATTR_EXT_BASE; // Not needed?
    in.size = rp_pkt.busaccess.len; // Size of entire packet + data
    in.width = 0;                   // Bus width (0 means SystemC will figure it out)
    in.stream_width = 0;            // Not sure
    in.byte_enable_len = 0;         // Not sure

    // Allocate space for the packet
	pkt.alloc(sizeof(rp_pkt.busaccess_ext_base) + rp_pkt.busaccess.len);

    // Encode the packet with teh data above and info about our peer
    size_t packet_length = rp_encode_busaccess(&peer, &pkt.pkt->busaccess_ext_base, &in);
    
    if (DEBUG) std::cerr << "Pkt Size: " << (int) sizeof(packet_length) << std::endl;
                        //  << "Data Size: " <<  (int) value_size << std::endl
                        //  << "Total Size: " << (int) data_size << std::endl;

    // Write the packet and data to the socket
    write_to_socket(pkt.pkt, packet_length);
    write_to_socket(values, rp_pkt.busaccess.len);    
}

// Double check that our  peer matches up with us
int check_peer() {
    // Have we populated our peer structure yet (recieved a hello packet from peer)
    if (!peer_updated) {
        std::cerr << "No hello packet, peer not updated" << std::endl;
        return 0;
    }

    // Major Remote Port version check
    if (peer.version.major != RP_VERSION_MAJOR) {
        std::cerr << "Major version mismatch with peer: " << peer.version.major << " (peer) != " << RP_VERSION_MAJOR << " (local)" << std::endl;
        return 0;
    }

    // Minor Remote Port version check
    if (peer.version.minor != RP_VERSION_MINOR) {
        std::cerr << "Minor version mismatch with peer: " << peer.version.minor << " (peer) != " << RP_VERSION_MINOR << " (local)" << std::endl;
        return 0;
    }

    // Check if the Extended Bus Access format is supported. This will not hinder communication, just ensure that we default to the base Bus Access packets
    if (peer.caps.busaccess_ext_base) {
        std::cerr << "Bus access extended base format not supported by peer" << std::endl;
    }

    return 1; // All good
}


int main(int argc, char *argv[]) {
    std::cerr << "Port Test v0.5:" << std::endl;

    // set socket path if given in command land argument
    if (argc > 1) socket_path = argv[1];
    std::cerr << "Socket: " << socket_path << std::endl;

    // Initalize our socket
    init_socket();

    // Wait for SystemC to connect
    wait_for_connection();

    // Double check to ensure we will be able to communicate
    int check_result = check_peer();
    if (!check_result) exit(1);

    unsigned long long addr = 0x40000100ULL; // Hard coded memory mapped address for demo
    int value = 0; // Value to write
    unsigned char* values_array; // Byte array of value data
    std::string line; // String provided by user

    while(1) {
        // Request and decode an integer value from the user. Then convert it to an array of bytes
        std::cerr << "Please enter a value (hex) to write to 0x" << std::hex << addr << ": ";
        std::getline(std::cin, line);
        value = std::stoi(line, 0, 16);
        values_array = static_cast<unsigned char*>(static_cast<void*>(&value));

        // Write that value to the SystemC memory
        write_mem_value(addr, values_array, 1);

        // Read the write response packet
        read_pkt_from_socket();

        // Attempt to then read that same value back
        read_mem_value(addr, values_array, 4);

        // Read the read response packet and data
        read_pkt_from_socket();

        // Wait between inputs for usability
        sleep(1);

        // TODO: Print the value recived value for verification
    }

    return 0;
}

// SystemC requires we have this function implemented
int sc_main(int argc, char* argv[]) { return 0; }
#pragma once

#include <sys/types.h>

#include "systemc.h"

#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/tlm_quantumkeeper.h"

#include "../libsystemctlm-soc/libremote-port/remote-port-tlm.h"
#include "../libsystemctlm-soc/libremote-port/remote-port-tlm-memory-master.h"
#include "../libsystemctlm-soc/libremote-port/remote-port-tlm-memory-slave.h"
#include "../libsystemctlm-soc/libremote-port/remote-port-tlm-wires.h"

constexpr int NUM_IRQ_WIRES = 1;

class Host : public remoteport_tlm {
    private:
        remoteport_tlm_wires rp_wires_in;

    public:
        remoteport_tlm_memory_master rp_m_axi_mem;
        // remoteport_tlm_memory_slave rp_s_axi_mem;

        // HW to Host interrupt signals
        sc_vector<sc_signal<bool> > hw2host_irq;

    public:
        // tlm_utils::simple_target_socket<remoteport_tlm_memory_slave> *s_axi_mem;
        tlm_utils::simple_initiator_socket<remoteport_tlm_memory_master> *m_axi_mem;

        Host(sc_core::sc_module_name name, const char *sk_descr, Iremoteport_tlm_sync *sync = NULL); // remoteport_tlm_sync_untimed_ptr
};

Host::Host(sc_core::sc_module_name name, const char *sk_descr, Iremoteport_tlm_sync *sync)
    : remoteport_tlm(name, -1, sk_descr, sync, false),
      // rp_s_axi_mem("rp_s_axi_mem"),
      rp_m_axi_mem("rp_m_axi_mem"),
      rp_wires_in("wires_in", NUM_IRQ_WIRES, 0),
      hw2host_irq("hw2host_irq", rp_wires_in.wires_in.size())
{   
    // HW to Host interrupt signals
    for (int i = 0; i < NUM_IRQ_WIRES; i++) {
		rp_wires_in.wires_in[i](hw2host_irq[i]);
	}

    // s_axi_mem = &rp_s_axi_mem.sk;
    // register_dev(0, &rp_s_axi_mem);
    m_axi_mem = &rp_m_axi_mem.sk;
    register_dev(0, &rp_m_axi_mem);
    register_dev(1, &rp_wires_in);
}
